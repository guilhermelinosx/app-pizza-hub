import React from 'react'
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native'

import { Feather } from '@expo/vector-icons'

interface ItemProps {
  data: {
    id: string
    product_id: string
    name: string
    amount: string | number
  }
  deleteItem: (item_id: string) => void
}

export function ListItem({ data, deleteItem }: ItemProps) {
  function handleDeleteItem() {
    deleteItem(data.id)
  }

  return (
    <View style={styles.container}>
      <View style={styles.contentAmount}>
        <Text style={styles.amount}>{data.amount}</Text>
      </View>

      <View style={styles.contentName}>
        <Text style={styles.name}>{data.name}</Text>
      </View>

      <TouchableOpacity onPress={handleDeleteItem}>
        <Feather name="trash-2" color="#F00" size={25} />
      </TouchableOpacity>
    </View>
  )
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-evenly',
    borderBottomWidth: 1,
    backgroundColor: '#fff',
    borderRadius: 8,
    padding: 10,
    marginBottom: 10
  },
  contentName: {
    flex: 1
  },
  name: {
    fontSize: 18,
    fontWeight: 'bold'
  },
  contentAmount: {
    paddingHorizontal: 10,
    width: 50,
    alignItems: 'center'
  },
  amount: {
    fontSize: 18,
    fontWeight: 'bold'
  }
})
