import React from 'react'
import {
  View,
  Text,
  Dimensions,
  TouchableOpacity,
  StyleSheet,
  ScrollView
} from 'react-native'
import { CategoryProps } from '../screens/order'

export type ModalPickerProps = {
  options: CategoryProps[]
  handleCloseModal: () => void
  handleSelect: (item: CategoryProps) => void
}

const { width, height } = Dimensions.get('window')

export const ModalPicker = ({
  options,
  handleCloseModal,
  handleSelect
}: ModalPickerProps) => {
  function onPress(item: CategoryProps) {
    handleSelect(item)
    handleCloseModal()
  }

  const option = options.map((item, index) => (
    <TouchableOpacity
      style={styles.option}
      key={index}
      onPress={() => onPress(item)}
    >
      <Text style={styles.optionText}>{item.name}</Text>
    </TouchableOpacity>
  ))

  return (
    <TouchableOpacity style={styles.container} onPress={handleCloseModal}>
      <View style={styles.content}>
        <ScrollView showsVerticalScrollIndicator={false}>{option}</ScrollView>
      </View>
    </TouchableOpacity>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center'
  },

  content: {
    backgroundColor: '#fff',
    borderRadius: 8,
    width: width * 0.9,
    height: height * 0.4,
    marginTop: height * 0.18,
    paddingHorizontal: 20,
    paddingVertical: 10,
    justifyContent: 'center',
    borderColor: '#1d1d1d',
    borderWidth: 1
  },

  option: {
    borderBottomWidth: 1,
    borderBottomColor: '#ccc',
    alignItems: 'center',
    height: 60,
    justifyContent: 'center'
  },

  optionText: {
    fontSize: 18,
    fontWeight: 'bold',
    color: '#1d1d1d'
  }
})
