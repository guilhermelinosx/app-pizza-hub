import axios from 'axios'

export const api = axios.create({
  baseURL: 'https://api-pizza-hub.up.railway.app'
})
