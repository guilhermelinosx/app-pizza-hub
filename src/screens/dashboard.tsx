import React from 'react'
import {
  Text,
  StyleSheet,
  TouchableOpacity,
  SafeAreaView,
  View,
  TextInput
} from 'react-native'
import { AuthContext } from '../contexts/auth.context'
import { useNavigation } from '@react-navigation/native'
import { ParamsList } from '../routes/app.route'
import { NativeStackNavigationProp } from '@react-navigation/native-stack'
import { FontAwesome } from '@expo/vector-icons'
import { api } from '../services/api'

export function Dashboard() {
  const navigation = useNavigation<NativeStackNavigationProp<ParamsList>>()
  const { signOut, user } = React.useContext(AuthContext)
  const [number, setNumber] = React.useState('')

  async function handleCreateOrder() {
    if (number === '') {
      alert('Number is required')
      return
    }

    try {
      const res = await api.post('/api/orders', {
        table: Number(number)
      })

      navigation.navigate('Order', {
        number: number,
        order_id: res.data.id
      })

      setNumber('')
    } catch (err) {
      console.log(`handleCreateOrder: ${err}`)
    }
  }

  const handleSignOut = () => {
    signOut()
  }

  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.head}>
        <Text style={styles.titleHead}>{user.name.split(' ')[0]}</Text>

        <TouchableOpacity onPress={handleSignOut}>
          <FontAwesome name="sign-out" size={30} color="#fff" />
        </TouchableOpacity>
      </View>

      <View style={styles.body}>
        <Text style={styles.titleBody}>New Order</Text>

        <TextInput
          style={[styles.inputBody]}
          placeholder={'Table Number'}
          keyboardType="numeric"
          value={number}
          onChangeText={setNumber}
        ></TextInput>

        <TouchableOpacity style={styles.buttonBody} onPress={handleCreateOrder}>
          <Text style={styles.buttonText}>Start Order</Text>
        </TouchableOpacity>
      </View>
    </SafeAreaView>
  )
}

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#1d1d1d',
    paddingHorizontal: 20,
    flexDirection: 'column'
  },

  head: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    width: '100%',
    marginTop: 40
  },

  titleHead: {
    fontSize: 30,
    fontWeight: 'bold',
    color: '#fff'
  },

  body: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
    marginTop: -100
  },

  titleBody: {
    fontSize: 30,
    color: '#FFF',
    fontWeight: 'bold'
  },

  inputBody: {
    width: '100%',
    height: 50,
    backgroundColor: '#fff',
    borderRadius: 8,
    marginTop: 20,
    padding: 10,
    fontSize: 18,
    fontWeight: 'bold',
    textAlign: 'center'
  },

  buttonBody: {
    width: '100%',
    height: 50,
    backgroundColor: '#f00',
    borderRadius: 8,
    marginTop: 20,
    padding: 10,
    alignItems: 'center',
    justifyContent: 'center'
  },

  buttonText: {
    color: '#fff',
    fontSize: 18,
    fontWeight: 'bold'
  }
})
