import { AuthContext } from '../contexts/auth.context'
import React from 'react'
import {
  Text,
  StyleSheet,
  View,
  TextInput,
  TouchableOpacity,
  ActivityIndicator,
  SafeAreaView
} from 'react-native'

export function SignIn() {
  const { signIn, loadingAuth } = React.useContext(AuthContext)
  const [email, setEmail] = React.useState('')
  const [password, setPassword] = React.useState('')

  const handleSignIn = async () => {
    if (email === '' || password === '') {
      alert('Email and password are required')
      return
    }

    await signIn({ email, password })
  }

  return (
    <SafeAreaView style={styles.container}>
      <Text style={styles.title}>Pizza Hub</Text>
      <Text style={styles.subtitle}>Welcome back!</Text>
      <View style={styles.form}>
        <>
          <TextInput
            style={[styles.input]}
            placeholder={'Email'}
            value={email}
            onChangeText={email => setEmail(email)}
            autoCapitalize="none"
            keyboardType="email-address"
          />

          <TextInput
            style={[styles.input]}
            placeholder={'Password'}
            value={password}
            onChangeText={password => setPassword(password)}
            secureTextEntry
          />

          <TouchableOpacity style={styles.button} onPress={handleSignIn}>
            {loadingAuth ? (
              <ActivityIndicator size={24} color="#fff" />
            ) : (
              <Text style={styles.buttonText}>Sign In</Text>
            )}
          </TouchableOpacity>
        </>
      </View>
    </SafeAreaView>
  )
}

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#1d1d1d',
    paddingHorizontal: 20
  },

  title: {
    fontSize: 40,
    fontWeight: 'bold',
    color: '#fff'
  },

  subtitle: {
    fontSize: 16,
    color: '#fff',
    marginBottom: 40,
    fontWeight: 'bold'
  },

  form: {
    width: '100%'
  },

  input: {
    height: 50,
    backgroundColor: '#fff',
    borderRadius: 8,
    fontSize: 16,
    fontWeight: 'bold',
    marginTop: 20,
    padding: 10
  },

  error: {
    marginLeft: 10,
    justifyContent: 'flex-start'
  },

  errorText: {
    color: '#f00',
    fontSize: 14
  },

  button: {
    width: '100%',
    height: 50,
    backgroundColor: '#f00',
    borderRadius: 8,
    marginTop: 20,
    padding: 10,
    fontWeight: 'bold',
    alignItems: 'center',
    justifyContent: 'center'
  },

  buttonText: {
    color: '#fff',
    fontSize: 18,
    fontWeight: 'bold'
  }
})
