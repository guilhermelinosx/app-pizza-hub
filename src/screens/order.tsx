import React, { useState, useEffect } from 'react'
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  TextInput,
  Modal,
  SafeAreaView,
  FlatList
} from 'react-native'
import { useRoute, RouteProp, useNavigation } from '@react-navigation/native'
import { Feather } from '@expo/vector-icons'
import { NativeStackNavigationProp } from '@react-navigation/native-stack'
import { api } from '../services/api'
import { ParamsList } from '../routes/app.route'
import { ModalPicker } from '../components/modalPicker'
import { ListItem } from '../components/listItems'

export type RouteDetailParams = {
  Order: {
    number: string | number
    order_id: string
  }
}

export type CategoryProps = {
  id: string
  name: string
}

export type ProductProps = {
  id: string
  name: string
}

export type ItemProps = {
  id: string
  product_id: string
  name: string
  amount: string | number
}

export type OrderRouteProps = RouteProp<RouteDetailParams, 'Order'>

export function Order() {
  const route = useRoute<OrderRouteProps>()
  const navigation = useNavigation<NativeStackNavigationProp<ParamsList>>()

  const [modalCategoryVisible, setModalCategoryVisible] = useState(false)
  const [category, setCategory] = useState<CategoryProps[] | []>([])
  const [categorySelected, setCategorySelected] = useState<
    CategoryProps | undefined
  >()

  const [modalProductVisible, setModalProductVisible] = useState(false)
  const [products, setProducts] = useState<ProductProps[] | []>([])
  const [productSelected, setProductSelected] = useState<
    ProductProps | undefined
  >()

  const [amount, setAmount] = useState('1')
  const [items, setItems] = useState<ItemProps[]>([])

  useEffect(() => {
    async function loadCategories() {
      try {
        const { data } = await api.get('/api/categories')

        setCategory(data)
        setCategorySelected(data[0])
      } catch (err) {
        console.log(`loadCategories: ${err}`)
      }
    }

    loadCategories()
  }, [])

  useEffect(() => {
    async function loadProducts() {
      try {
        if (categorySelected) {
          const { data } = await api.get(`/api/categories/products`, {
            params: {
              category_id: categorySelected.id
            }
          })

          setProducts(data)
          setProductSelected(data[0])
        }
      } catch (err) {
        console.log(`loadProducts: ${err}`)
      }
    }

    loadProducts()
  }, [categorySelected])

  function handleChangeCategory(item: CategoryProps) {
    setCategorySelected(item)
  }

  function handleChangeProduct(item: ProductProps) {
    setProductSelected(item)
  }

  async function handleCloseOrder() {
    try {
      await api.delete(`/api/orders/${route.params.order_id}`)

      navigation.goBack()
    } catch (err) {
      console.log(`handleCloseOrder: ${err}`)
    }
  }

  async function handleAddItem() {
    try {
      const { data } = await api.post('/api/orders/add', {
        order_id: route.params.order_id,
        product_id: productSelected?.id,
        amount: Number(amount)
      })

      const item = {
        id: data.id,
        product_id: productSelected?.id as string,
        name: productSelected?.name as string,
        amount: amount
      }

      setItems(oldArray => [...oldArray, item])
    } catch (err) {
      console.log(`handleAddItem: ${err}`)
    }
  }

  async function handleDeleteItem(item_id: string) {
    try {
      await api.delete(`/api/orders/remove/${item_id}`)

      setItems(oldArray => oldArray.filter(item => item.id !== item_id))
    } catch (err) {
      console.log(`handleDeleteItem: ${err}`)
    }
  }

  async function handleFinishOrder() {
    try {
      navigation.navigate('Finish', {
        order_id: route.params?.order_id,
        number: route.params?.number
      })
    } catch (err) {
      console.log(`handleFinish: ${err}`)
    }
  }

  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.header}>
        <Text style={styles.titleHeader}>Table {route.params.number}</Text>
        {items.length === 0 && (
          <TouchableOpacity onPress={handleCloseOrder}>
            <Feather name="trash-2" size={28} color="#f00" />
          </TouchableOpacity>
        )}
      </View>

      <View style={styles.content}>
        {category.length !== 0 && (
          <TouchableOpacity
            style={styles.input}
            onPress={() => setModalCategoryVisible(true)}
          >
            <Text style={styles.titleBody}>{categorySelected?.name}</Text>
          </TouchableOpacity>
        )}

        {products.length !== 0 && (
          <TouchableOpacity
            style={styles.input}
            onPress={() => setModalProductVisible(true)}
          >
            <Text style={styles.titleBody}>{productSelected?.name}</Text>
          </TouchableOpacity>
        )}

        <View style={styles.quantity}>
          <Text style={styles.titleQuantity}>Quantity</Text>
          <TextInput
            style={[styles.inputQuantity, {}]}
            keyboardType="numeric"
            value={amount}
            onChangeText={setAmount}
          />
        </View>

        <View style={styles.action}>
          <TouchableOpacity style={styles.buttonAction} onPress={handleAddItem}>
            <Text style={styles.titleBody}>+</Text>
          </TouchableOpacity>

          <TouchableOpacity
            style={[
              styles.buttonActionConfirm,
              { opacity: items.length === 0 ? 0.3 : 1 }
            ]}
            disabled={items.length === 0}
            onPress={handleFinishOrder}
          >
            <Text style={styles.titleBody}>Advance</Text>
          </TouchableOpacity>
        </View>
      </View>

      <FlatList
        showsVerticalScrollIndicator={false}
        style={styles.list}
        data={items}
        keyExtractor={item => item.id}
        renderItem={({ item }) => (
          <ListItem data={item} deleteItem={handleDeleteItem} />
        )}
      />

      <Modal
        transparent={true}
        visible={modalCategoryVisible}
        animationType="none"
      >
        <ModalPicker
          handleCloseModal={() => setModalCategoryVisible(false)}
          options={category}
          handleSelect={handleChangeCategory}
        />
      </Modal>

      <Modal
        transparent={true}
        visible={modalProductVisible}
        animationType="none"
      >
        <ModalPicker
          handleCloseModal={() => setModalProductVisible(false)}
          options={products}
          handleSelect={handleChangeProduct}
        />
      </Modal>
    </SafeAreaView>
  )
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#1d1d1d',
    paddingHorizontal: 20
  },

  header: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    width: '100%',
    marginTop: 40,
    marginBottom: 100
  },

  titleHeader: {
    fontSize: 30,
    fontWeight: 'bold',
    color: '#fff'
  },

  content: {
    flex: 1,
    flexDirection: 'column',
    width: '100%',
    marginTop: -100
  },

  input: {
    width: '100%',
    height: 50,
    backgroundColor: '#fff',
    borderRadius: 8,
    fontWeight: 'bold',
    marginTop: 20,
    padding: 10,
    justifyContent: 'center'
  },

  titleBody: {
    fontSize: 18,
    fontWeight: 'bold',
    color: '#1d1d1d'
  },

  quantity: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    width: '100%',
    paddingLeft: 10
  },

  titleQuantity: {
    fontSize: 18,
    fontWeight: 'bold',
    color: '#fff',
    marginTop: 20
  },

  inputQuantity: {
    width: '75%',
    height: 50,
    backgroundColor: '#fff',
    borderRadius: 8,
    fontWeight: 'bold',
    marginTop: 20,
    fontSize: 18,
    padding: 10,
    justifyContent: 'center',
    textAlign: 'center'
  },

  action: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    width: '100%',
    marginTop: 20
  },

  buttonAction: {
    width: '24%',
    height: 50,
    backgroundColor: '#f00',
    borderRadius: 8,
    justifyContent: 'center',
    alignItems: 'center'
  },

  buttonActionConfirm: {
    width: '73%',
    height: 50,
    backgroundColor: '#008000',
    borderRadius: 8,
    justifyContent: 'center',
    alignItems: 'center'
  },

  list: {
    width: '100%',
    flex: 1
  }
})
