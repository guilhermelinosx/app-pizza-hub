import React from 'react'

import {
  Text,
  StyleSheet,
  SafeAreaView,
  TouchableOpacity,
  View
} from 'react-native'
import { RouteProp, useNavigation, useRoute } from '@react-navigation/native'
import { api } from '../services/api'
import { NativeStackNavigationProp } from '@react-navigation/native-stack'
import { ParamsList } from '../routes/app.route'

export type RouteDetailParams = {
  Finish: {
    number: string | number
    order_id: string
  }
}

export type FinishRouteProps = RouteProp<RouteDetailParams, 'Finish'>

export function Finish() {
  const route = useRoute<FinishRouteProps>()
  const navigation = useNavigation<NativeStackNavigationProp<ParamsList>>()

  async function handleFinish() {
    try {
      await api.put(`/api/orders/send/${route.params.order_id}`)

      navigation.popToTop()
    } catch (err) {
      console.log(`handleFinish: ${err}`)
    }
  }

  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.content}>
        <Text style={[styles.title, { fontSize: 40 }]}>
          Do you want to complete the order?
        </Text>
        <Text style={[styles.title, { fontSize: 32 }]}>
          Table {route.params.number}
        </Text>

        <TouchableOpacity style={styles.button} onPress={handleFinish}>
          <Text style={styles.titleButton}>Finish</Text>
        </TouchableOpacity>
      </View>
    </SafeAreaView>
  )
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#1d1d1d',
    paddingHorizontal: 20
  },

  content: {
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'column',
    flex: 1,
    marginTop: -100
  },

  title: {
    fontWeight: 'bold',
    color: '#fff',
    textAlign: 'center',
    marginBottom: 30
  },
  button: {
    width: '100%',
    height: 50,
    backgroundColor: '#f00',
    borderRadius: 8,
    marginTop: 10,

    justifyContent: 'center',
    alignItems: 'center'
  },
  titleButton: {
    fontSize: 20,
    fontWeight: 'bold',
    color: '#fff'
  }
})
