import React from 'react'
import { createNativeStackNavigator } from '@react-navigation/native-stack'
import { SignIn } from '../screens/signIn'

const { Navigator, Screen } = createNativeStackNavigator()

export const AuthRoutes = () => {
  return (
    <Navigator>
      <Screen
        name="SignIn"
        component={SignIn}
        options={{
          headerShown: false,
          contentStyle: {
            backgroundColor: 'transparent'
          }
        }}
      />
    </Navigator>
  )
}
